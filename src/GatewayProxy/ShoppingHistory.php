<?php
namespace GatewayProxy;

/**
 * Description of ShoppingHistory
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class ShoppingHistory
{

    /**
     *
     * @var \GatewayProxy\CartGateway $gateway 
     */
    private $gateway;

    /**
     *
     * @var Array $shoopingCartIds 
     */
    private $shoppingCartIds = [];

    /**
     * 
     * @param \GatewayProxy\CartGateway $gateway
     * @param Array $ids
     */
    public function __construct(CartGateway $gateway, $ids = [])
    {
        $this->gateway = $gateway;
        $this->shoppingCartIds = $ids;
    }

    /**
     * ShoppingCarts
     * 
     * @return Array
     */
    public function listAllCarts()
    {
        $shoppingCarts = [];
        foreach ($this->shoppingCartIds as $id) {
            $shoppingCarts[] = $this->gateway->retrieve($id);
        }
        return $shoppingCarts;
    }
}
