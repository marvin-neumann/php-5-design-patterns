<?php
namespace GatewayProxy;

/**
 * Description of InMemoryCart
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class InMemoryCart implements CartGatewayInterface
{
    private $listOfCarts = [];
    
    public function getIdOfRecordedCart()
    {
        return end($this->listOfCarts);
    }

    public function persist(ShoppingCart $cart)
    {
        $this->listOfCarts[] = $cart;
    }

    public function retrieve($id)
    {
        return $this->listOfCarts[$id];
    }
}
