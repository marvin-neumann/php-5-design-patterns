<?php
namespace GatewayProxy;

/**
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
interface CartGatewayInterface
{

    public function persist(ShoppingCart $cart);

    public function retrieve($id);

    public function getIdOfRecordedCart();
}
