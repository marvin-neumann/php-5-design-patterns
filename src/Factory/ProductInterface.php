<?php
namespace Factory;

/**
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
interface ProductInterface
{

    public function getPrice();

    public function getPicture();

    public function getDescription();
}
