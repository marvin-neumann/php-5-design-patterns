<?php
namespace Factory;

use Factory\Keyboard;
use Factory\Mouse;

/**
 * ProductFactory will produce a product
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class ProductFactory
{

    /**
     * Make product
     * 
     * @param int $productId
     * @return Mouse|Keyboard
     */
    public function make($productId)
    {
        if ($this->isKeyboard($productId)) {
            $product = new Keyboard();
        } else {
            $product = new Mouse();
        }
        return $product;
    }

    /**
     * Is it a Keyboard?
     * 
     * @param int $productId
     * @return bool
     */
    public function isKeyboard($productId)
    {
        return substr($productId, 0, 1) == 'k';
    }
}
