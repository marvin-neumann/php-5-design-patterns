<?php
namespace Factory;

/**
 * Description of Mouse
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class Mouse implements ProductInterface
{

    public function getDescription()
    {
        return 'Mouse';
    }

    public function getPicture()
    {
        return NULL;
    }

    public function getPrice()
    {
        return 10;
    }
}
