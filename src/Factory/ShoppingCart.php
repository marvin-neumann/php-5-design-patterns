<?php
namespace Factory;

use Factory\ProductFactory;

/**
 * Description of ShoppingCart
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class ShoppingCart
{

    /**
     * Products in shopping cart
     * 
     * @var array
     */
    private $products = [];

    /**
     *
     * @var ProductFactory 
     */
    private $productFactory;

    public function __construct()
    {
        $this->productFactory = new ProductFactory();
    }

    /**
     * Add product
     * 
     * @param int $productId
     */
    public function add($productId)
    {
        $this->products[] = $this->productFactory->make($productId);
    }
}
