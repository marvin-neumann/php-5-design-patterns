<?php
namespace Factory;

/**
 * Description of Keyboard
 *
 * @author Marvin Neumann <marvin.neumann@gmail.com>
 */
class Keyboard implements ProductInterface
{

    public function getDescription()
    {
        return 'Keyboard';
    }

    public function getPicture()
    {
        return NULL;
    }

    public function getPrice()
    {
        return 20;
    }
}
